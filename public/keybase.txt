==================================================================
https://keybase.io/danehkar
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://danehkar.gitlab.io
  * I am danehkar (https://keybase.io/danehkar) on keybase.
  * I have a public key with fingerprint 1DC1 0ED0 100C 32D4 6719  9CF9 C27E C064 763A EEAD

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "010116f827ffbe9a8ccdb62b3e8ad9caf9732b854dd0cf5b02ce732dcd947f0c3a5e0a",
      "fingerprint": "1dc10ed0100c32d467199cf9c27ec064763aeead",
      "host": "keybase.io",
      "key_id": "c27ec064763aeead",
      "kid": "010116f827ffbe9a8ccdb62b3e8ad9caf9732b854dd0cf5b02ce732dcd947f0c3a5e0a",
      "uid": "d3dd8ed0ae9902a8f185a5c3d5629b19",
      "username": "danehkar"
    },
    "service": {
      "hostname": "danehkar.gitlab.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1669190909,
  "expire_in": 157680000,
  "prev": "5da775a83b8694bb0ae025f010b182f64ad643507f843558354cc480beb9242a",
  "seqno": 49,
  "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.1.13
Comment: https://keybase.io/crypto

yMIcAnicrVJLSFRRGL5jPqfSFkEbg+aGiDDafT+G3GgPadcielAM53XHi9Pcce7V
yUxcVJotKjJIQtNZKUrYwwLJfFOQZpBCBCIJFb5DJKKH1LlWm2jZOYuf85/v+/7/
//hHt25ivB4FZGqVDRMvPGNDtysYVDO1Xs1CC1exgWq2jGwEEsbEdoJlJmYDLMdz
PK8YmqAaBiQ60BDCUBGgSDSAdQQMXRUFqMkSxhwyZMgJiNAMRliXVINDIpAJB1g/
a5iREIlFY2bEobI8RjxHMFWnEAFLisrrOjJ0JKgEcYqkKiIgBGBKLLVsl0Gbg8Am
BaZFc/QR3GjvH/j/3HfFhhwWMdZov4DoOicAzeA1GchIxLIi6JDXXaBNYhFwmrho
ECGlZSDG1vhZmq00EXGNdSf5C1EQMp0wgL+misYsx0JWmP6XOk7UDrh8pyrqEuIE
Bn9LBaEZwdRNyqgkMdu0ImyAp0jkmK42ryg6r3P0+llyJmrGSNB0EbKqaBw9bh1S
SSVlDFRVBpoINUWXIKTDcYJsUOcgrwmGIgGsSKLMqYZGg6yJsoSQpHGQQF2QBNcc
m5RHLDYg0VIOCFFN2wxFgFMRI2zN8ODJZMbjZVJTktw1Y7wZ2/4sXwKnM6PZrcPv
oL8w8ePDhRuKE5jl8sSDizl5xWJrxupSeL7xeIrv4f3DTCq3Gmqy+touDdWV+x9n
7Ri/KARnQtlHmmZGvtQ+WD8wv+q501sQP7+3Nx6Wc0cWPnends4nb5/LnCgsiU74
Ers71/TR5wOXr9nx/KyOye78+jdL9if1xKGbXVf68tPSC0rWvo7P3u1Kayje3P90
8WzS9/6VtvbkIs/15vXl+q7ajuSkfb62Zl/Prbpp+dSucz7/1MPBlRz25ep0YmS5
Jy98NB00L3hb3r/eM8fEVwbvbbn6rGPnWOH+Rjnc8CTj46vliqJ2nhnIayHFb7+V
mZM9x3of5XqLfgIHbUkY
=zQPY
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/danehkar

==================================================================
